package Ejercicio;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class Ej2 extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField numero;
	private JTextField resultado;
	private JLabel lblNewLabel_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ej2 frame = new Ej2();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ej2() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(Color.YELLOW);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Ingrese n\u00FAmero");
		lblNewLabel.setBounds(44, 43, 97, 14);
		contentPane.add(lblNewLabel);
		
		numero = new JTextField();
		numero.setBounds(44, 68, 86, 20);
		contentPane.add(numero);
		numero.setColumns(10);
		
		resultado = new JTextField();
		resultado.setBounds(44, 178, 86, 20);
		contentPane.add(resultado);
		resultado.setColumns(10);
		
		JButton btnNewButton = new JButton("Calcular");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			int Numero;
			Numero=Integer.parseInt(numero.getText());
			
			if(Numero % 2 ==0) {
				resultado.setText("Par");
			}
			if(Numero % 2 !=0) {
				resultado.setText("Impar");
			}
			}
		});
		btnNewButton.setBounds(224, 108, 89, 23);
		contentPane.add(btnNewButton);
		
		lblNewLabel_1 = new JLabel("El n\u00FAmero es");
		lblNewLabel_1.setBounds(45, 153, 96, 14);
		contentPane.add(lblNewLabel_1);
	}
}
