package Ejercicio;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class Ej6 extends JFrame {

	private JPanel contentPane;
	private JTextField curso;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ej6 frame = new Ej6();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ej6() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(Color.CYAN);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Usted pertenece a");
		lblNewLabel.setBounds(154, 29, 89, 14);
		contentPane.add(lblNewLabel);
		
		curso = new JTextField();
		curso.setBounds(133, 84, 126, 44);
		contentPane.add(curso);
		curso.setColumns(10);
		
		JButton btnNewButton = new JButton("Valorar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			int Curso;
			Curso=Integer.parseInt(curso.getText());
			if (Curso==0) {
				curso.setText("Jard�n de infantes");
			}
			if (0<Curso & Curso<7) {
				curso.setText("Primaria");
			}
			if (7<=Curso & Curso<13) {
				curso.setText("Secundaria");
			}
			
			}
		});
		btnNewButton.setBounds(154, 178, 89, 23);
		contentPane.add(btnNewButton);
	}

}
