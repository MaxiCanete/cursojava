package Ej1;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class EJ1 extends JFrame {

	private JPanel contentPane;
	private JTextField A;
	private JTextField B;
	private JTextField C;
	private JTextField D;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EJ1 frame = new EJ1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EJ1() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(Color.ORANGE);
		contentPane.setForeground(Color.LIGHT_GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("1er Nota");
		lblNewLabel.setBounds(32, 11, 46, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("2da Nota");
		lblNewLabel_1.setBounds(32, 88, 46, 14);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("3er Nota");
		lblNewLabel_2.setBounds(32, 177, 46, 14);
		contentPane.add(lblNewLabel_2);
		
		A = new JTextField();
		A.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		A.setBounds(32, 36, 86, 20);
		contentPane.add(A);
		A.setColumns(10);
		
		B = new JTextField();
		B.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		B.setBounds(32, 126, 86, 20);
		contentPane.add(B);
		B.setColumns(10);
		
		C = new JTextField();
		C.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		C.setBounds(32, 211, 86, 20);
		contentPane.add(C);
		C.setColumns(10);
		
		D = new JTextField();
		D.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		D.setBounds(258, 85, 86, 20);
		contentPane.add(D);
		D.setColumns(10);
		
		JButton btnNewButton = new JButton("Calcular");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			int a,b,c,d;
			a=Integer.parseInt(A.getText());
			b=Integer.parseInt(B.getText());
			c=Integer.parseInt(C.getText());
			d=((a+b+c)/3);
				if(d==7){
					D.setText("Aprobado");
				}else {
					D.setText("Desaprobado");
				}
			}
		});
		btnNewButton.setBounds(255, 177, 89, 23);
		contentPane.add(btnNewButton);
	}

}
