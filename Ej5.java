package Ejercicio;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class Ej5 extends JFrame {

	private JPanel contentPane;
	private JTextField posicion;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ej5 frame = new Ej5();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ej5() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(Color.PINK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Posicion obtenida");
		lblNewLabel.setBackground(Color.PINK);
		lblNewLabel.setBounds(159, 43, 83, 14);
		contentPane.add(lblNewLabel);
		
		posicion = new JTextField();
		posicion.setBounds(121, 83, 164, 53);
		contentPane.add(posicion);
		posicion.setColumns(10);
		
		JButton btnNewButton = new JButton("Verificar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			int Posicion;
			Posicion=Integer.parseInt(posicion.getText());
			if (Posicion==1) {
				posicion.setText("Obtuviste la medalla de oro");
			}
			if (Posicion==2) {
				posicion.setText("Obtuviste la medalla de plata");
			}
			if (Posicion==3) {
				posicion.setText("Obtuviste la medalla de bronce");
			}
			if (Posicion>3) {
				posicion.setText("Segui participando");
			}
				
				
			}
			
			
		});
		btnNewButton.setBounds(151, 179, 103, 23);
		contentPane.add(btnNewButton);
		
	}
}
